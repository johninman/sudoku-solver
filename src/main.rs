extern crate sudoku_solver;

use std::env;
use sudoku_solver::board::Board;
use sudoku_solver::solve;

fn main() {
    let args: Vec<String> = env::args().collect();
    let file = &args.get(1).expect("Please provide a file");
    let mut board = Board::from_file(file).unwrap();
    solve(&mut board);
    println!("{}", board);
}
